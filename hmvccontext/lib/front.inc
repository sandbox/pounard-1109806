<?php

/**
 * Front controller not only can handle a controller stack to execute but also
 * keeps track of the current context scope.
 * 
 * Whenever a new context is being dispatched, it must be registered as the
 * current context so arbitrary code has a static entry point for fetching
 * the current context scope.
 * 
 * This particular implementation supports controller stacking while processing
 * another controller. This feature is quite dumb and should probably not be
 * kept.
 * 
 * From a Drupal point of view, front controller is actually the menu router
 * tied to the menu_get_item() call in the menu_execute_active_handler()
 * function
 */
class DrupalControllerFront
{
  /**
   * Singleton pattern implementation.
   * 
   * @var DrupalControllerFront
   */
  private static $__instance;

  /**
   * Get singleton instance.
   * 
   * @return DrupalControllerFront
   */
  public static function getInstance() {
    if (!isset(self::$__instance)) {
      self::$__instance = new DrupalControllerFront;
    }
    return self::$__instance;
  }

  /**
   * Singleton pattern implementation.
   */
  private function __construct() {}

  /**
   * @var DrupalRequestHttp
   */
  protected $_request;

  /**
   * Set current request instance.
   * 
   * @param DrupalRequestHttp $request
   * 
   * @return DrupalControllerFront
   */
  public function setRequest(DrupalRequestHttp $request) {
    $this->_request = $request;
    return $this;
  }

  /**
   * Get current response.
   * 
   * Will return default implementation if none set.
   * 
   * @return DrupalResponseInterface
   */
  public function getRequest() {
    if (!isset($this->_request)) {
      $this->_request = new DrupalRequestHttp;
    }
    return $this->_request;
  }

  /**
   * @var DrupalResponseInterface
   */
  protected $_response;

  /**
   * Set current response instance.
   * 
   * @return DrupalControllerFront
   */
  public function setResponse(DrupalResponseInterface $response) {
    $this->_response = $response;
    return $this;
  }

  /**
   * Get current response.
   * 
   * Will return default implementation if none set.
   * 
   * @return DrupalResponseInterface
   */
  public function getResponse() {
    if (!isset($this->_response)) {
      $this->_response = new DrupalResponsePage;
    }
    return $this->_response;
  }

  /**
   * @var DrupalDispatcherInterface
   */
  protected $_dispatcher;

  /**
   * Set dispatcher.
   * 
   * @param DrupalDispatcherInterface $dispatcher
   * 
   * @return DrupalControllerFront
   */
  public function setDispatcher(DrupalDispatcherInterface $dispatcher) {
    $this->_dispatcher = $dispatcher;
    $this->_dispatcher
      ->setRequest($this->getRequest())
      ->setResponse($this->getResponse());
    return $this;
  }

  /**
   * Get dispatcher.
   * 
   * @return DrupalDispatcherInterface
   */
  public function getDispatcher() {
    if (!isset($this->_dispatcher)) {
      $this->setDispatcher(new DrupalDispatcher);
    }
    return $this->_dispatcher;
  }

  /**
   * Internal controller stack.
   * 
   * @var array
   *   Array of DrupalControllerInterface
   */
  protected $_controllers = array();

  /**
   * @var DrupalControllerInterface
   */
  protected $_currentController;

  /**
   * Get current controller running.
   * 
   * @return DrupalControllerInterface
   * 
   * @throws Exception
   *   If dispatch loop not started.
   */
  public function getCurrentController() {
    if (!isset($this->_currentController)) {
      throw new Exception("Dispatch loop is not initialized yet.");
    }
    return $this->_currentController;
  }

  /**
   * Set current controller.
   * 
   * @param DrupalControllerInterface $controller
   * 
   * @return DrupalControllerFront
   */
  public function setCurrentController(DrupalControllerInterface $controller) {
    $this->_currentController = $controller;
    return $this;
  }

  /**
   * Run the dispatch loop.
   * 
   * @return DrupalControllerFront
   */
  public function run() {
    try {
      $dispatcher = $this->getDispatcher();

      foreach ($this->_controllers as $controller) {
        $dispatcher->dispatch($controller);
      }
    }
    catch (Exception $e) {
      $this->getResponse()->setError($e);
    }

    return $this;
  }

  /**
   * Dispatch single controller.
   * 
   * @param DrupalControllerInterface $controller
   */
  public function dispatch(DrupalControllerInterface $controller, $intercept = FALSE) {
    return $this->getDispatcher()->dispatch($controller, $intercept);
  }

  /**
   * Add a controller to current stack position.
   * 
   * New controllers may be stacked while running using the upper algorithm,
   * even if I'm not really sure this makes sense.
   * 
   * @param DrupalControllerInterface $controller
   * 
   * @return DrupalControllerFront
   */
  public function stack(DrupalControllerInterface $controller) {
    $this->_controllers[] = $controller;
    return $this;
  }
}
