<?php

/**
 * Base class for contextes.
 */
abstract class DrupalContextBusinessBase implements DrupalContextBusinessInterface
{
  protected $_value;

  public function getValue() {
    return $this->_value;
  }

  public function vary() {
    return FALSE;
  }
}

/**
 * Single controller interface. Controllers may be stacked each one into
 * another.
 */
interface DrupalControllerInterface
{
  /**
   * Set parent controller.
   * 
   * @param DrupalControllerInterface $controller
   */
  public function setParent(DrupalControllerInterface $controller);

  /**
   * Get parent controller.
   * 
   * @return DrupalControllerInterface
   */
  public function getParent();

  /**
   * Convenience method that tell if the current instance as the asked
   * context attached.
   * 
   * @param string $name
   * 
   * @return bool
   */
  public function hasContext(string $name);

  /**
   * Get context by name.
   * 
   * If this controller has no associated context, ask its parent in order
   * order to find one.
   * 
   * @param string $name
   * 
   * @return DrupalContextBusinessInterface
   *   If context found, NULL else.
   */
  public function getContext($name);

  /**
   * Add specific business context to the current controller.
   * 
   * @param DrupalContextBusinessInterface $context
   */
  public function addContext(DrupalContextBusinessInterface $context);

  /**
   * Set request.
   * 
   * @param DrupalRequestInterface $request
   * 
   * @return DrupalControllerInterface
   */
  public function setRequest(DrupalRequestInterface $request);

  /**
   * Get request.
   * 
   * @return DrupalRequestInterface
   */
  public function getRequest();

  /**
   * Set response.
   * 
   * @param DrupalResponseInterface $response
   * 
   * @return DrupalControllerInterface
   */
  public function setResponse(DrupalResponseInterface $response);

  /**
   * Get response.
   * 
   * @return DrupalResponseInterface
   */
  public function getResponse();

  /**
   * Pre dispatch hook for specific implementations.
   * 
   * Pre dispatch seems the right place where to set additional context
   * business stuff before caching happens.
   * 
   * Pre dispatch does not goes to cache.
   */
  public function preDispatch();

  /**
   * Post dispatch hook for specific implementations.
   * 
   * Post dispatch does not goes to cache.
   */
  public function postDispatch();

  /**
   * Initialize and dispatch child controller.
   * 
   * @param DrupalControllerInterface $controller
   * 
   * @return mixed
   *   View.
   */
  public function dispatchChild(DrupalControllerInterface $controller);

  /**
   * Tell if this controller result can be cached or not.
   * 
   * @return bool
   *   Default is TRUE, specific implementation may change this.
   */
  public function isCachable();

  /**
   * Get cache identifier, if cachable.
   * 
   * @return string
   */
  public function getCacheIdentifier();

  /**
   * If cachable, fetch cache table.
   * 
   * This will allow later cache clean.
   * 
   * @return string
   */
  public function getCacheBin();

  /**
   * Function where the real business stuff happens.
   * 
   * @return mixed
   *   View.
   */
  public function runAction();
}

/**
 * Null object pattern implementation.
 * 
 * When a top level controller is reached, and has no parent, it has the
 * responsability to use this particular instance as parent in order to
 * avoid crashes and let specific code chaining the getParent() calls
 * without raising exceptions.
 */
class DrupalControllerNull implements DrupalControllerInterface
{
  /**
   * Singleton pattern implementation.
   * 
   * @var DrupalControllerNull
   */
  private static $__instance;

  /**
   * Get singleton instance.
   * 
   * @return DrupalControllerNull
   */
  public static function getInstance() {
    if (!isset(self::$__instance)) {
      self::$__instance = new DrupalControllerNull;
    }
    return self::$__instance;
  }

  /**
   * Singleton pattern implementation.
   */
  private function __construct() {}

  public function setParent(DrupalControllerInterface $controller) {
    return $this;
  }

  public function getParent() {
    return $this->_parent;
  }

  public function getContext($name) {
    return NULL;
  }

  public function addContext(DrupalContextBusinessInterface $context) {}

  public function hasContext(string $name) {
    return FALSE;
  }

  protected $_request;

  public function setRequest(DrupalRequestInterface $request) {
    $this->_request = $request;
    return $this;
  }

  public function getRequest() {
    return $this->_request;
  }

  protected $_response;

  public function setResponse(DrupalResponseInterface $response) {
    $this->_response = $response;
    return $this;
  }

  public function getResponse() {
    return $this->_response;
  }

  public function preDispatch() {}

  public function postDispatch() {}

  public function dispatchChild(DrupalControllerInterface $controller) {}

  public function isCachable() {
    return FALSE;
  }

  public function getCacheIdentifier() {
    return NULL;
  }

  public function getCacheBin() {
    return NULL;
  }

  public function runAction() {
    return NULL;
  }
}

abstract class DrupalControllerBase implements DrupalControllerInterface
{
  /**
   * @var DrupalControllerInterface
   */
  protected $_parent;

  public function setParent(DrupalControllerInterface $controller) {
    $this->_parent = $controller;
    $this->_request = $controller->getRequest();
    $this->_response = $controller->getResponse();
    return $this;
  }

  public function getParent() {
    // If no parent set, return the current DrupalControllerNull singleton.
    if (!isset($this->_parent)) {
      return DrupalControllerNull::getInstance();
    }
    return $this->_parent;
  }

  protected $_contextes = array();

  public function hasContext(string $name) {
    return isset($name) || $this->getParent()->hasContext($name);
  }

  public function getContext($name) {
    if (isset($this->_contextes[$name])) {
      return $this->_contextes[$name];
    }
    return $this->getParent()->getContext($name);
  }

  public function addContext(DrupalContextBusinessInterface $context) {
    $this->_contextes[$context->getName()] = $context;
  }

  /**
   * @var DrupalRequestInterface
   */
  protected $_request;

  public function setRequest(DrupalRequestInterface $request) {
    $this->_request = $request;
    return $this;
  }

  public function getRequest() {
    if (!isset($this->_request)) {
      throw new Exception("No request set.");
    }
    return $this->_request;
  }

  protected $_response;

  public function setResponse(DrupalResponseInterface $response) {
    $this->_response = $response;
    return $this;
  }

  public function getResponse() {
    if (!isset($this->_response)) {
      throw new Exception("No response set.");
    }
    return $this->_response;
  }

  public function preDispatch() {}

  public function postDispatch() {}

  public function dispatchChild(DrupalControllerInterface $controller) {
    $controller->setParent($this);
    return DrupalControllerFront::getInstance()->dispatch($controller, TRUE);
  }

  public function isCachable() {
    foreach ($this->_contextes as $context) {
      if ($context->vary()) {
        return FALSE;
      }
    }
    return TRUE;
  }

  public function getCacheIdentifier() {
    // Default goes to asked path and a mix with contextes.

    // Cache identifier should also be dependent of:
    //  - Controller class name (more than one can be run in the same page)
    //  - Eventually: theme (but since most should return render array, this is
    //    not necessarly true).
    //  - Eventually: user session, this totally depend on business stuff being
    //    done in this controller. For example, a lot of block can have a global
    //    caching policy which means no user here.

    $temp = array();
    foreach ($this->_contextes as $key => $context) {
      $temp[$key] = (string) $context->getValue();
    }

    // Sort context keys for better chances of cache hit.
    ksort($temp);

    return $this->getRequest()->get('q') . ':' . md5(serialize($temp));
  }

  public function getCacheBin() {
    return 'cache';
  }
}
