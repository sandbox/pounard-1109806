<?php

/**
 * Dispatcher.
 * 
 * Using dependency injection, you will be able to attache ESI behavior right
 * here.
 */
interface DrupalDispatcherInterface
{
  /**
   * Set request.
   * 
   * @param DrupalRequestInterface $request
   * 
   * @return DrupalDispatcherInterface
   */
  public function setRequest(DrupalRequestInterface $request);

  /**
   * Get request.
   * 
   * @return DrupalRequestInterface
   */
  public function getRequest();

  /**
   * Set response.
   * 
   * @param DrupalResponseInterface $request
   * 
   * @return DrupalDispatcherInterface
   */
  public function setResponse(DrupalResponseInterface $response);

  /**
   * Get response.
   * 
   * @return DrupalResponseInterface
   */
  public function getResponse();

  /**
   * Run controller action.
   * 
   * @param DrupalControllerInterface $controller
   * @param bool $intercept = FALSE
   *   (optional) If set to TRUE, the dispatcher will return the build array
   *   instead of stacking it to the current response object. 
   * 
   * @return mixed
   *   NULL if $intercept is FALSE, otherwise the controller returned view.
   */
  public function dispatch(DrupalControllerInterface $controller, $intercept = FALSE);
}

class DrupalDispatcher implements DrupalDispatcherInterface
{
  /**
   * @var DrupalRequestInterface
   */
  protected $_request;

  public function setRequest(DrupalRequestInterface $request) {
    $this->_request = $request;
    return $this;
  }

  public function getRequest() {
    return $this->_request;
  }

  /**
   * @var DrupalResponseInterface
   */
  protected $_response;

  public function setResponse(DrupalResponseInterface $response) {
    $this->_response = $response;
    return $this;
  }

  public function getResponse() {
    return $this->_response;
  }

  public function dispatch(DrupalControllerInterface $controller, $intercept = FALSE) {
    // Allow procedural code to fetch back current controller using the front
    // controller as main entry point.
    DrupalControllerFront::getInstance()->setCurrentController($controller);

    $controller
      ->setRequest($this->getRequest())
      ->setResponse($this->getResponse())
      ->preDispatch();

    $view =  $controller->runAction();

    $controller->postDispatch();

    if (!$intercept) {
      $this->getResponse()->append('content', $view);
    }
    else {
      return $view;
    }
  }
}

class DrupalDispatcherCachable extends DrupalDispatcher
{
  public function dispatch(DrupalControllerInterface $controller, $intercept = FALSE) {
    // Allow procedural code to fetch back current controller using the front
    // controller as main entry point.
    DrupalControllerFront::getInstance()->setCurrentController($controller);

    $view = NULL;

    $controller
      ->setRequest($this->getRequest())
      ->setResponse($this->getResponse())
      ->preDispatch();

    // FIXME: The controller itself should handle its own cache, since some
    // others could be dispatched while running. This means that the front
    // controller should probably get over this and handle caching itself
    // by computing a cache identifier using the full controller stack that
    // has been run since this one started.
    // By doing this, the lowest cachable controller would give the general
    // caching policy for its parents and ensure caching maximisation while
    // no outdated data got cached.
    // Nevertheless, the problem exists only for top level elements, most
    // blocks, for example, should not embed other controllers. The more
    // controllers hierarchy will be complex, the less the whole thing will
    // be cachable.
    // Therefore caching policy here, if ESI is enabled can be über
    // aggressive and totally ignore hierarchy because controllers would be
    // rendered ponctally outside the hierarchy itself just by repeating
    // parameters from the query in the <esi:*> tags.
    if ($cachable = $controller->isCachable()) {
      $cacheId = $controller->getCacheIdentifier();
      $cacheBin = $controller->getCacheBin();
    }

    if ($cachable && ($cached = cache_get($cacheId, $cacheBin))) {
      $view = $cached->data;
    }
    else {
      $view = $controller->runAction();

      if ($cachable) {
        cache_set($cacheId, $view, $cacheBin);
      }
    }

    $controller->postDispatch();

    if (!$intercept) {
      $this->getResponse()->append('content', $view);
    }
    else {
      return $view;
    }
  }
}

class DrupalDispatcherESI extends DrupalDispatcherCachable
{
  public function dispatch(DrupalControllerInterface $controller, $intercept = FALSE) {
    if (!$controller->isCachable()) {
      return parent::dispatch($controller, $intercept);
    }

    // Allow procedural code to fetch back current controller using the front
    // controller as main entry point.
    DrupalControllerFront::getInstance()->setCurrentController($controller);

    $view = NULL;

    $controller
      ->setRequest($this->getRequest())
      ->setResponse($this->getResponse());

    $cacheId = $controller->getCacheIdentifier();
    $cacheBin = $controller->getCacheBin();

    $view = array(
      '#prefix' => "<!--esi\n",
      '#suffix' => "\n-->",
      '#type' => 'html_tag',
      '#tag' => 'esi:include',
      '#attributes' => array(
        'src' => 'someurl&someparamstorebuildtheblock',
        'maxwait' => 5000,
      ),
    );

    if (!$intercept) {
      $this->getResponse()->append('content', $view);
    }
    else {
      return $view;
    }
  }
}
