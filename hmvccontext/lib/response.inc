<?php

/**
 * Reponse handler.
 */
interface DrupalResponseInterface
{
  /**
   * Set or unset the production mode. Production mode never let errors being
   * displayed to end user while debug mode would let the developer now what
   * happened.
   * 
   * @param bool $toggle
   *   TRUE to enable error display. FALSE is the default.
   * 
   * @return DrupalResponseInterface
   */
  public function setDisplayErrors($toggle);

  /**
   * Tells if we are on an erroneous query.
   * 
   * @return bool
   */
  public function isError();

  /**
   * An critical error happened. Set it to the response object. It will set
   * the current response as erroneous and may break execution flow.
   * 
   * @param mixed $exception
   *   Either an exception, or anything castable as string.
   * 
   * @return DrupalResponseInterface
   */
  public function setError($exception);

  /**
   * Add header.
   * 
   * @param string $header
   * @param string $value
   * @param string $replace = FALSE
   * 
   * @return DrupalResponseInterface
   */
  public function setHeader($header, $value, $replace = FALSE);

  /**
   * Get particular header.
   * 
   * @param string $header
   * 
   * @return array
   *   Array of values for this header, ordered, NULL if none found. 
   */
  public function getHeader($header);

  /**
   * Get all headers.
   * 
   * @return array
   */
  public function getHeaders();

  /**
   * Send headers.
   * 
   * @return DrupalResponseInterface
   */
  public function sendHeaders();

  /**
   * Set redirect.
   * 
   * @param string $url
   * @param int $code
   * 
   * @return DrupalResponseInterface
   */
  public function setRedirect($url, $code = 302);

  /**
   * Tell if the current response ends up with a redirect.
   */
  public function isRedirect();

  /**
   * Append data to content queue.
   * 
   * @param string $name
   * @param string $data
   * 
   * @return DrupalResponseInterface
   */
  public function append($name, $data);

  /**
   * Prepend data to content queue.
   * 
   * @param string $name
   * @param string $data
   * 
   * @return DrupalResponseInterface
   */
  public function prepend($name, $data);

  /**
   * Send response.
   * 
   * @return DrupalResponseInterface
   */
  public function send();
}

/**
 * Base implementation.
 */
class DrupalResponseHttp implements DrupalResponseInterface
{
  protected $_displayErrors = FALSE;

  public function setDisplayErrors($toggle) {
    $this->_displayErrors = (bool) $toggle;
    return $this;
  }

  protected $_errors = array();

  public function isError() {
    return !empty($this->_errors);
  }

  public function setError($exception) {
    $this->_errors[] = $exception;
    return $this;
  }

  protected function _displayError($exception) {
    $build = array();

    if ($exception instanceof Exception) {
      $build[] = array(
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
        '#markup' => $exception->getMessage(),
      );
      $build[] = array(
        '#prefix' => '<pre>',
        '#suffix' => '</pre>',
        '#markup' => $exception->getTraceAsString(),
      );
    }
    else {
      $build['#markup'] = (string) $exception;
    }

    return drupal_render($build);
  }

  public function setHeader($header, $value, $replace = FALSE) {
    // Right now, core has a good implementation for this.
    drupal_add_http_header($header, $value, !$replace);
    return $this;
  }

  public function getHeader($header) {
    // Right now, core has a good implementation for this.
    return drupal_get_http_header($header);
  }

  public function getHeaders() {
    // Right now, core has a good implementation for this.
    return drupal_get_http_header();
  }

  public function sendHeaders() {
    if ($this->isError()) {
      drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
      drupal_add_http_header('Status', '500 Internal Server Error');
    }
    else if (!$this->isRedirect()) {
      // FIXME: TODO.      
    }
    else {
      // Right now, core has a good implementation for this.
      drupal_add_http_header('Content-Type', 'text/html; charset=utf-8');
      drupal_page_header();
    }
    return $this;
  }

  /**
   * @var string
   */
  protected $_redirectUrl;

  /**
   * @var int
   */
  protected $_redirectCode = 302;

  public function setRedirect($url, $code = 302) {
    $this->_redirectUrl = $url;
    $this->_redirectCode = $code;
    return $this;
  }

  public function isRedirect() {
    return isset($this->_redirectUrl);
  }

  protected $_content = array();

  public function append($name, $data) {
    $this->_content[$name][] = $data;
    return $this;
  }

  public function prepend($name, $data) {
    if (!isset($this->_content[$name])) {
      $this->_content[$name][] = $data;
    }
    else {
      array_unshift($this->_content[$name], $data);
    }
    return $this;
  }

  public function send() {

    if ($this->isError()) {
      if ($this->_displayErrors) {
        foreach ($this->_errors as $exception) {
          print $this->_displayError($exception);
        }
      }
      else {
        print '500 Internal Server Error';
      }
    }

    if (!$this->isRedirect()) {
      // Do not display anything. If 30x header was correct, the browser already
      // did redirect elsewhere.
    }

    else { 
      foreach ($this->_content as $name => $elements) {
        foreach ($elements as $element) {
          print render($element);
        }
      }
    }

    return $this;
  }
}

/**
 * Specific implementation that handles page.
 * 
 * Incomplete implementation, let's hope it works.
 */
class DrupalResponsePage extends DrupalResponseHttp
{
  public function send() {

    if ($this->isError()) {
      // FIXME: For page mode, the maintenance page would be better.
      if ($this->_displayErrors) {
        foreach ($this->_errors as $exception) {
          print $this->_displayError($exception);
        }
      }
      else {
        print '500 Internal Server Error';
      }
    }

    else if ($this->isRedirect()) {
      // Do not display anything. If 30x header was correct, the browser already
      // did redirect elsewhere.
    }

    else { 
      $build = array();

      foreach ($this->_content as $name => $elements) {
        foreach ($elements as $element) {
          if (!is_array($element)) {
            $build[$name][]['#markup'] = $element;
          }
          else {
            $build[$name][] = $element;
          }
        }
      }

      print drupal_render_page($build);
    }

    return $this;
  }
}
