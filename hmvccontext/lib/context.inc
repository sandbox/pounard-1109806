<?php

/**
 * Name and value will be used in order to compute cache identifiers and
 * predictible URL for internal caching or AJAX/ESI mecanisms direct access.
 */
interface DrupalContextBusinessInterface
{
  /**
   * Get context key.
   */
  public function getName();

  /**
   * Get current context value.
   * 
   * @return NULL
   *   If no value set.
   */
  public function getValue();

  /**
   * Return TRUE if context produces random information that might trouble
   * a caching mecanism.
   */
  public function vary();
}
