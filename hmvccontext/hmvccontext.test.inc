<?php

/**
 * Node context.
 */
class FooNodeContext extends DrupalContextBusinessBase
{
  public function getName() {
    return 'node';
  }

  /**
   * Get current fully loaded node.
   * 
   * @return object
   */
  public function getNode() {
    return node_load($this->_value);
  }

  /**
   * Get current node identifier.
   * 
   * @return int
   */
  public function getNid() {
    return $this->_value;
  }

  /**
   * Build context instance.
   * 
   * @param int $nid
   *   Context node identifier.
   */
  public function __construct($nid) {
    $this->_value = $nid;
  }
}

/**
 * OG context.
 * 
 * Extension here is only for convenience because this code assume that OG
 * will always be a group, which is not the real use case.
 */
class FooOgContext extends FooNodeContext
{
  public function getName() {
    return 'og';
  }

  /**
   * Get current OG context depending on the given node context.
   * 
   * @return FooOgContext
   *   OG context instance, or NULL if node has no OG tied.
   */
  public static function getInstanceFromNode(FooNodeContext $node) {
    // Foo code, here the OG context should attempt to find a matching
    // OG depending on the given node.
    // In real life, OG is a field which can be tied to any entity, therefore
    // node here is unrevelant.
    return new FooOgContext($node->getNid());
  }
}

/**
 * Mock controller C.
 */
class DrupalControllerFooC extends DrupalControllerBase
{
  public function runAction() {
    $build = array();
    $build[]['#markup'] = "C::runAction()\n";

    if ($nodeContext = $this->getContext('node')) {
      $build[]['#markup'] = "  C has node : " . $nodeContext->getNid() . "\n";
    }
    if ($ogContext = $this->getContext('og')) {
      $build[]['#markup'] = "  C has group : " . $ogContext->getNid() . "\n";
    }

    return $build;
  }
}

/**
 * Sample of how to fetch context outside the controller itself.
 */
function controller_foo_b_business_function() {
  $build = array();
  $controller = DrupalControllerFront::getInstance()->getCurrentController();

  if ($nodeContext = $controller->getContext('node')) {
    $build[]['#markup'] = "  B procedural callback has node : " . $nodeContext->getNid() . "\n";
  }
  if ($ogContext = $controller->getContext('og')) {
    $build[]['#markup'] = "  B procedural callback has group : " . $ogContext->getNid() . "\n";
  }

  return $build;
}

/**
 * Mock controller B.
 */
class DrupalControllerFooB extends DrupalControllerBase
{
  public function preDispatch() {
    // B Got a specific context, which is node.
    $nodeContext = new FooNodeContext(21);
    $this->addContext($nodeContext);
    // But no OG group.
  }

  public function runAction() {
    $build = array();
    $build[]['#markup'] = "B::runAction()\n";

    // FIXME: Uncomment this for fun.
    // throw new Exception("oups");

    if ($nodeContext = $this->getContext('node')) {
      $build[]['#markup'] = "  B has node : " . $nodeContext->getNid() . "\n";
    }
    if ($ogContext = $this->getContext('og')) {
      $build[]['#markup'] = "  B has group : " . $ogContext->getNid() . "\n";
    }

    $build[] = controller_foo_b_business_function();

    // Stack and run C controller.
    $build[] = $this->dispatchChild(new DrupalControllerFooC);

    return $build;
  }
}

/**
 * Sample of how to fetch context outside the controller itself.
 */
function controller_foo_a_business_function() {
  $build = array();

  $controller = DrupalControllerFront::getInstance()->getCurrentController();

  if ($nodeContext = $controller->getContext('node')) {
    $build[]['#markup'] = "  A procedural callback has node : " . $nodeContext->getNid() . "\n";
  }
  if ($ogContext = $controller->getContext('og')) {
    $build[]['#markup'] = "  A procedural callback has group : " . $ogContext->getNid() . "\n";
  }

  // Stack and run B controller.
  $build[] = $controller->dispatchChild(new DrupalControllerFooB);

  return $build;
}

/**
 * Mock controller A.
 */
class DrupalControllerFooA extends DrupalControllerBase
{
  public function preDispatch() {
    // A Got a specific context, which is node.
    $nodeContext = new FooNodeContext(12);
    $this->addContext($nodeContext);
    // And a tied OG group.
    $this->addContext(FooOgContext::getInstanceFromNode($nodeContext));
  }

  public function runAction() {
    $build = array();
    $build['#prefix'] = "<pre>";
    $build['#suffix'] = "</pre>";
    $build[]['#markup'] = "A::runAction()\n";

    if ($nodeContext = $this->getContext('node')) {
      $build[]['#markup'] = "  A has node : " . $nodeContext->getNid() . "\n";
    }
    if ($ogContext = $this->getContext('og')) {
      $build[]['#markup'] = "  A has group : " . $ogContext->getNid() . "\n";
    }

    $build[] = controller_foo_a_business_function();
    return $build;
  }
}

function hmvccontext_live_test_page_callback() {
  // Initialize the front controller.
  $response = DrupalControllerFront::getInstance()
    // This one is optional (see front controller code).
    // ->setRequest(new DrupalRequestHttp)
    ->setResponse(new DrupalResponsePage)
    //->setDispatcher(new DrupalDispatcherESI)
    ->stack(new DrupalControllerFooA)
    ->run()
    ->getResponse();

  // print_r($response);die();

  $response
    ->setDisplayErrors(TRUE)
    ->sendHeaders()
    ->send();
}
