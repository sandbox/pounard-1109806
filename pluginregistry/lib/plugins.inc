<?php

/**
 * Exception thrown when trying to fetch a non existing plugin from the
 * factory.
 */
class NotRegisteredPluginException extends PluginException {}

/**
 * Exception thrown when trying to fetch a non existing factory.
 */
class NotRegisteredFactoryException extends PluginException {}

/**
 * Exception thrown when trying to fetch an non typed object from an
 * empty registry.
 */
class EmptyRegistryPluginException extends PluginException {}

/**
 * Factory interface.
 * 
 * Per default, we will give a default factory for any registered plugin type,
 * but in some extraordinary cases, modules would want to define their own
 * factory instead.
 * 
 * Plugin factory implement both the registry and factory pattern. We call it
 * factory more than registry because most of the developers won't probably
 * use it as a registry in everyday code.
 */
interface DrupalPluginFactoryInterface
{
  /**
   * Get plugin factory type name.
   * 
   * @return string
   */
  public function getPluginFactoryType();

  /**
   * Get an instance of the registry.
   * 
   * Some needs may be covered by a site wide plugin type, in this particular
   * case the first defined and existing plugin class will be used for
   * instanciation.
   * 
   * @param string $type = NULL
   *   If left to NULL, will give the first implementation in the list.
   * @param ConfigInterface $options = NULL
   *   If given, will be used to initialiaze the given instance.
   * 
   * @return DrupalPluginInterface
   * 
   * @throws NotRegisteredPluginException
   */
  public function getInstance($pluginType = NULL, ConfigInterface $options = NULL);
}


/**
 * Static helper for fetching plugin factories.
 * 
 * Plugin factories are lazy instanciated singletons.
 */
class DrupalPluginRegistry extends DrupalInfoRegistry
{
  /**
   * Singleton pattern implementation.
   * 
   * @var DrupalPluginRegistry
   */
  protected static $_instance;

  /**
   * @var array
   */
  protected $_instances = array();

  /**
   * Get the factory for plugin type.
   * 
   * @param string $factoryType
   *   Internal plugin factory type.
   * 
   * @return DrupalPluginFactoryInterface
   * 
   * @throws NotRegisteredFactoryException
   */
  public static function getFactory($factoryType) {
    if (!isset(self::$_instance)) {
      self::$_instance = new DrupalPluginRegistry;
    }
    return self::$_instance->_getFactory($factoryType);
  }

  /**
   * Set to protected to differenciate it from the static method upper.
   * 
   * @param string $factoryType
   *   Internal plugin factory type.
   * 
   * @return DrupalPluginFactoryInterface
   * 
   * @throws NotRegisteredFactoryException
   */
  protected function _getFactory($factoryType) {
    if (isset($this->_instances[$factoryType])) {
      return $this->_instances[$factoryType];
    }
    else if (!isset($this->_cache[$factoryType])) {
      throw new NotRegisteredFactoryException("Plugin factory type '" . $factoryType . "' is not registered.");
    }
    else if (!isset($this->_cache[$factoryType]['class'])) {
      return new DrupalPluginFactory($factoryType, $this->_cache[$factoryType]);
    }
    else if (!class_exists($this->_cache[$factoryType]['class'])) {
      throw new NotRegisteredFactoryException("Plugin factory type '" . $factoryType . "' class '" . self::$_cache[$factoryType]['class'] . "' does not exists.");
    }
    else {
      return new $this->_cache[$factoryType]['class'];
    }
  }

  /**
   * Singleton pattern implementation.
   */
  protected function __construct() {
    $this->_setHook('plugin_type_info');
    $this->_loadCache();
  }
}

/**
 * Base implementation for Drupal plugin factory.
 * 
 * Suitable for most usages.
 */
class DrupalPluginFactory
  extends DrupalInfoRegistry
  implements DrupalPluginFactoryInterface
{
  /**
   * @var string
   */
  protected $_factoryType;

  /**
   * Get plugin type name.
   * 
   * @return string
   */
  public function getPluginFactoryType() {
    return $this->_factoryType;
  }

  /**
   * Null object pattern class.
   * 
   * @var string
   */
  protected $_nullObjectClass;

  /**
   * @var DrupalPluginInterface
   */
  protected $_nullObjectInstance;

  /**
   * Get an instance of the registry.
   * 
   * Some needs may be covered by a site wide plugin type, in this particular
   * case the first defined and existing plugin class will be used for
   * instanciation.
   * 
   * @param string $type = NULL
   *   If left to NULL, will give the first implementation in the list.
   * @param ConfigInterface $options = NULL
   *   If given, will be used to initialiaze the given instance.
   * 
   * @return DrupalPluginInterface
   * 
   * @throws NotRegisteredPluginException
   */
  public function getInstance($pluginType = NULL, ConfigInterface $options = NULL) {
    if (!isset($pluginType)) {
      if (empty($this->_cache)) {
        throw new EmptyRegistryPluginException("Plugin factory '" . $this->_factoryType . "' is empty.");
      }
      else {
        reset($this->_cache);
        $info = current($this->_cache);
        return $this->getInstance($info['type'], $info);
      }
    }
    else if (!isset($this->_cache[$pluginType]) || !isset($this->_cache[$pluginType])) {
      if (!isset($this->_nullObjectInstance)) {
        // Use a singleton pattern for null object instances.
        if (isset($this->_nullObjectClass) && class_exists($this->_nullObjectClass)) {
          $this->_nullObjectInstance = new $this->_nullObjectClass;
        }
        else {
          throw new NotRegisteredPluginException("Plugin type '" . $pluginType . "' is undefined in factory type '" . $this->_factoryType . "'.");
        }
      }
      return $this->_nullObjectInstance;
    }
    else if (!class_exists($this->_cache[$pluginType]['class'])) {
      throw new NotRegisteredPluginException("Plugin class '" . $this->_cache[$pluginType] . "' for plugin type '" . $pluginType . "' is undefined in factory type '" . $this->_factoryType . "'.");
    }
    else {
      $instance = new $this->_cache[$pluginType]['class']($options);
      return $instance;
    }
  }

  /**
   * Construct the registry.
   * 
   * @param string $factoryType
   * @param array $info
   */
  public function __construct($factoryType, $info) {
    $this->_factoryType = $factoryType;
    $this->_setHook(isset($info['hook']) ? $info['hook'] : 'plugin_' . $factoryType . '_info');
    if (isset($info['dummy'])) {
      $this->_nullObjectClass = $info['dummy'];
    }
    $this->_loadCache();
  }
}
