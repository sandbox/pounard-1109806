<?php

/**
 * Base registry implementation.
 */
class DrupalInfoRegistry
{
  /**
   * @var array
   */
  protected $_cache;

  /**
   * @var string
   */
  protected $_hook;

  /**
   * Set hook name.
   * 
   * Implementing classes should call it from their constructor.
   * 
   * @param string $hook
   */
  protected function _setHook($hook) {
    $this->_hook = $hook;
  }

  /**
   * Let's set cachable to FALSE per default. Building info array is no more
   * than some module_invoke() being called, fetching a cache from a distant
   * storage could cost more than this in a lot of cases.
   * 
   * @var bool
   */
  protected $_cachable = FALSE;

  /**
   * Does this info registry should cache its items?
   * 
   * @param bool $toggle
   */
  protected function _setCachable($toggle) {
    $this->_cachable = (bool) $toggle;
  }

  /**
   * Ensure cache is loaded of factory info has been built.
   */
  protected function _loadCache() {
    if (!isset($this->_cache)) {
      if ($this->_cachable) {
        // FIXME: Attempt loading.
        // FIXME: Should be in a else statement.
        // FIXME: If we got something here, just:
        // return;
      }

      $this->_cache = array();

      foreach (module_implements($this->_hook) as $module) {
        foreach (module_invoke($module, $this->_hook) as $type => $info) {
          $this->registerItem($type, $info, TRUE, TRUE);
        }
      }

      drupal_alter($this->_hook, $this->_cache);

      if ($this->_cachable) {
        // FIXME: Store cache.
      }
    }
  }  
  
  /**
   * Register single item.
   * 
   * This is public and it allows modules to arbitrary register itms when
   * they need it. It's nevertheless non recommanded, and registries should
   * probably always be registered with hook_ITEM_TYPE_info().
   * 
   * @see hook_ITEM_TYPE_info()
   * 
   * @param string $type
   * @param array $info
   * @param bool $forceUpdate = FALSE
   * @param bool $disableCacheUpdate = FALSE
   */
  public function registerItem($type, $info, $forceUpdate = FALSE, $disableCacheUpdate = FALSE) {
    // Consistency check, this will allow intelligent caching, 
    if ($forceUpdate || !isset($this->_cache[$type])) {

      $this->_cache[$type] = $info;
      $this->_cache[$type]['type'] = $type;

      if (!$disableCacheUpdate) {
        // FIXME: Use a lock.
        // FIXME: Save the cache.
        // FIXME: Release the lock.
      }
    }
  }
}
