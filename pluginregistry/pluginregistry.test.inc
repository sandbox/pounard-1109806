<?php

class MockTypeBFactory extends DrupalPluginFactory
{
  // Nothing to do, this is here for fun only.
}

interface JobInterface {
  public function sayMyJob();
}

class JobHunter extends PluginExportable implements JobInterface
{
  public function sayMyJob() {
    return "Hello, I am a hunter and I'm gonna kill!\n";
  }
}

class JobTeacher extends PluginExportable implements JobInterface
{
  public function sayMyJob() {
    return "I am a teacher, and you should behave!\n";
  }
}

class JobNull extends PluginStateless implements JobInterface
{
  public function sayMyJob() {
    return "I have no job, because I'm a null object.\n";
  }
}

class PluginATypeA extends PluginExportable
{
}

function pluginregistry_test_page_callback() {
  $message = '';

  try {
    $hunter = DrupalPluginRegistry::getFactory('job')->getInstance('hunter');
    $message .= "Hunter says: " . $hunter->sayMyJob();
  }
  catch (PluginException $e) {
    $message .= "Could not fetch the hunter job, this is an error: " . $e->getMessage() . "\n";
  }

  try {
    $teacher = DrupalPluginRegistry::getFactory('job')->getInstance('teacher');
    $message .= "Teacher says: " . $teacher->sayMyJob();
  }
  catch (PluginException $e) {
    $message .= "Could not fetch the teacher job, this is an error: " . $e->getMessage() . "\n";
  }

  try {
    $santa_claus = DrupalPluginRegistry::getFactory('job')->getInstance('santa_claus');
    $message .= "Santa claus says: " . $santa_claus->sayMyJob();
  }
  catch (PluginException $e) {
    $message .= "Could not fetch the santa clause job, this is an error: " . $e->getMessage() . "\n";
  }

  return array(
    '#prefix' => '<pre>',
    '#suffix' => '</pre>',
    '#markup' => $message,
  );
}
