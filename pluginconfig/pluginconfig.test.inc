<?php

class NullPlugin extends PluginExportable {}

class NullFactory extends PluginFactory
{
  public function __construct() {
    $this->_types['null'] = 'NullPlugin';
  }
}

function pluginconfig_test_page_callback() {
  $content = array();

  $test = array(
    'foo1' => 'bar2',
    'baz3' => array(
      'foo4' => array(
        'bar5' => 'baz6',
      ),
      'foo7' => 'bar9',
    ),
    'baz9' => NULL,
  );

  $uri = 'temporary://test.js';

  if (!file_exists($uri)) {
    file_put_contents($uri, json_encode($test));
  }

  try {
    $reader = new ExportableReaderFileJson($uri);
    $config = $reader->read();
    $content[]['#markup'] = '<pre>' . print_r($config, TRUE) . '</pre>';

    $factory = new NullFactory;
    $plugin = $factory->getPlugin('null');
    $content[]['#markup'] = '<pre>' . print_r($plugin, TRUE) . '</pre>';

    $factory = new NullFactory;
    $otherPlugin = $factory->getPlugin('null', $config);
    $content[]['#markup'] = '<pre>' . print_r($otherPlugin, TRUE) . '</pre>';

    try {
      $nonExisting = $factory->getPlugin('foo', $config);
      throw new Exception("WTF!");
    }
    catch (PluginException $e) {
      $content[]['#markup'] = "<p>Yes! Got an exception.</p>";
    }
  }
  catch (Exception $e) {
    $content[]['#markup'] = "<strong>Exception happened, message is: " . $e->getMessage() . "</strong>";
    $content[]['#markup'] = "<pre>" . $e->getTraceAsString() . "</pre>";
  }

  return $content;
}
