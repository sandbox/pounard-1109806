<?php

/**
 * @package Exportable
 */
class ExportableException extends Exception {}

/**
 * @package Exportable
 */
interface ConfigReaderInterface
{
  /**
   * Get ConfigInterface instance.
   *
   * @return ConfigInterface
   * 
   * @throws ExportableException
   */
  public function read();
}

/**
 * @package Exportable
 */
interface ConfigWriterInterface
{
  /**
   * Get ConfigInterface instance.
   *
   * @param ConfigInterface $config
   * 
   * @throws ExportableException
   */
  public function write(ConfigInterface $config);
}

/**
 * @package Exportable
 */
interface ExportableInterface
{
  /**
   * Get object definition and configuration as a ConfigInterface object.
   * 
   * @return ConfigInterface
   */
  public function getConfig();
}

/**
 * Common function for file based reader and writer.
 * 
 * @package Exportable
 */
abstract class ExportableFile
{
  protected $_uri;

  /**
   * Default constructor.
   * 
   * @param string $uri
   * 
   * @throws ExportableException
   */
  public function __construct($uri) {
    $scheme = explode('://', $uri, 2);
    $scheme = array_shift($scheme);

    if (!file_stream_wrapper_valid_scheme($scheme)) {
      throw new ExportableException("Invalid file scheme: '" .  $uri. "'.");
    }

    $this->_uri = $uri;
  }
}

/**
 * Common base for config file reader.
 * 
 * @package Exportable
 */
abstract class ExportableReaderFile extends ExportableFile
{
  /**
   * Get current file contents.
   * 
   * @return string
   */
  protected function _getBuffer() {
    return file_get_contents($this->_uri);
  }

  /**
   * Default constructor.
   * 
   * @param string $uri
   * 
   * @throws ExportableException
   */
  public function __construct($uri) {
    parent::__construct($uri);

    if (!file_exists($uri)) {
      throw new ExportableException("File does not exists: '" .  $uri. "'.");
    }
  }
}

/**
 * JSON config file reader.
 * 
 * @package Exportable
 */
class ExportableReaderFileJson extends ExportableReaderFile
{
  public function read() {
    if (!$data = json_decode($this->_getBuffer(), TRUE)) {
      throw new ExportableException("File does not contains valid JSON: '" . $this->_uri . "'.");
    }

    return new ConfigArray($data);
  }
}
