<?php

/**
 * @package PluginManagement
 */
class PluginException extends Exception {}

/**
 * Basic plugin interface.
 * 
 * @package PluginManagement
 */
interface PluginInterface
{
  /**
   * Default constructor.
   * 
   * @param ConfigInterface $config = NULL
   *   (optional) Plugin configuration.
   */
  public function __construct(ConfigInterface $config = NULL);
}

/**
 * System dependent plugin interface.
 * 
 * @package PluginManagement
 */
interface PluginCriticalInterface extends PluginInterface
{
  /**
   * Tell if the current component can run of the actual system.
   * 
   * @return bool
   */
  public function isHealthy();
}

/**
 * Exportable implementation of plugin.
 * 
 * @package PluginManagement
 */
abstract class PluginExportable implements PluginInterface, ExportableInterface
{
  protected $_config;

  public function __construct(ConfigInterface $config = NULL) {
    $this->_config = isset($config) ? $config : new ConfigArray;
  }

  public function getConfig() {
    return $this->_config;
  }
}

/**
 * Defines a stateless plugin (that drops configuration on instanciation).
 * 
 * @package PluginManagement
 */
abstract class PluginStateless implements PluginInterface
{
  public function __construct(ConfigInterface $config = NULL) {}
}

/**
 * Default plugin factory.
 * 
 * @package PluginManagement
 */
class PluginFactory
{
  protected $_types = array();

  /**
   * Get plugin instance.
   * 
   * @param string $type = NULL
   *   (optional) Plugin type name.
   * @param ConfigInterface $config = NULL
   *   (optional) Plugin configuration.
   * 
   * @return PluginInterface
   * 
   * @throws PluginException
   */
  public function getPlugin($type = NULL, ConfigInterface $config = NULL) {
    if (!isset($this->_types[$type])) {
      throw new PluginException("Plugin type '" . $type . "' does not exists.");
    }

    $class = $this->_types[$type];

    if (!class_exists($class)) {
      throw new PluginException("Plugin type '" . $type . "' does not exists: cannot find class '" . $class . "'.");
    }

    return new $class($config);
  }
}
