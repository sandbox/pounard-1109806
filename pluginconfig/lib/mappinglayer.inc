<?php

/**
 * @package MapingLayer
 */
class ConfigException extends Exception {}

/**
 * @package MapingLayer
 */
interface ConfigInterface extends Traversable
{
  /**
   * Get value or subtree.
   * 
   * @param string $name
   * 
   * @return scalar|ConfigInterface
   */
  public function __get($name);

  /**
   * Set a value or subtree.
   * 
   * @param string $name
   * @param scalar|ConfigInterface|Array
   *   Value or subtree.
   * 
   * @throws ConfigException
   */
  public function __set($name, $value);

  /**
   * Get the full internal tree as a multidimentional array filled up with
   * scalar values.
   */
  public function toArray();
}

/**
 * Array based implementation of ConfigInterface.
 * 
 * Notice: PHP need the IteratorAggregate being declared here before the
 * Traversable interface, else it will fail.
 * 
 * @package Mapping layer
 */
class ConfigArray implements IteratorAggregate, ConfigInterface
{
  protected $_data = array();

  public function __get($name) {
    return isset($this->_data[$key]) ? $this->_data[$key] : NULL;
  }

  public function __set($name, $value) {
    if (!isset($value)) {
      $this->_data[$name] = NULL;
    }
    else if (is_scalar($value)) {
      $this->_data[$name] = $value;
    }
    else if (is_array($value) || $value instanceof Traversable) {
      $this->_data[$name] = new self($value);
    }
    else {
      throw new ConfigException("Not a scalar value or a Traversable instance.");
    }
  }

  public function getIterator() {
    return new ArrayIterator($this->_data);
  }

  public function toArray() {
    $array = array();
    foreach ($this->_data as $key => $value) {
      if ($value instanceof ConfigInterface) {
        $array[$key] = $value->toArray();
      }
      else {
        $array[$key] = $value;
      }
    }
    return $array;
  }

  /**
   * Default constructor.
   * 
   * @param array|Traversable $data = NULL
   *   (optional) Default values, all leaf must be scalar values.
   * 
   * @throws ConfigException
   */
  public function __construct($data = NULL) {
    if (is_array($data) || $data instanceof Traversable) {
      foreach ($data as $key => $value) {
        $this->$key = $value;
      }
    }
    else if (isset($data)) {
      throw new ConfigException("Not a scalar value or a Traversable instance in constructor.");
    }
  }
}
